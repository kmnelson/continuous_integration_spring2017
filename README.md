# Continuous Integration Technology Presentation

Continuous integration (CI) is a software development practice intended to make  bugs easier to find and remove. It involves:
* Integrating changes as early as possible
* Automatically building code after each integration
* After a successful build, test cases are run on the code


## Getting Started

If you don't have git or java installed locally on your computer make sure you install them.
This tutorial is designed to be run from the command line.
Do not use the gitlab site to commit files!
The goal is to get to the point where integrating and testing code is as easy as a single command line prompt, not going through a user interface.

There are two parts to this tutorial, doing them in order will be beneficial.
To get started, perform the following commands in a terminal window:

```
git clone https://gitlab.com/kmnelson/continuous_integration_spring2017.git
cd continuous_integration_spring2017                                      
git branch <your wm user id>          
git checkout <your wm user id>
cp scripts/TEMPLATE.sh scripts/<your wm user id>.sh
cp Testcases/src/test/testcases_TEMPLATE.java Testcases/src/test/testcases_<your wm user id>.java
cp Factorial/src/test/buggedFactorial_TEMPLATE.java Factorial/src/test/buggedFactorial_<your wm user id>.java
```

This creates a copy of the codebase on your computer in a git tracked directory, creates your personal branch, and copies the template files to ones that you may modify.
These commands are fairly crucial.  In continuous integration you might have several collaborators accessing the same code.
In this tutorial, each of your classmates will create their own branch and create their own copy of the template script, code, and test cases.
If you modify the template script, template code, or template test cases you will be modifying it for everyone.  
Be sure to copy the script to its own file and then add your code to the copy specified by your userid.

This tutorial is designed to throw you at a project where you have a lot of collaborators making frequent commits.
As such, their code will appear on your branch after they push it to master and you pull from master.
Just ignore it!  We want you to get the feel for being in a large project but you are graded what goes in the files designated by your user id.

#### Git Script 
The first assignment is to familiarize yourself with git by creating a script to automate the workflow of continuous integration.
Good news!  The script already performs some functions.
It automatically runs your test code file (which is currently empty) and can stop you from committing if you have failed any of your tests.

*Note: this was written on a Mac for a Unix system.

If you are running Windows we recommend logging on the the department machines.

Alternatively, install Git Bash*

Included in the repository is a simple git script which, when completed, combines and automates commiting and adding changes to the main repository with testing code.  
Additionally it keeps the master branch up to date with your personal branch. 
This script is incomplete, you will need to edit it by adding several simple commands. 
This is intended to familiarize you with git and help with the second part of the tutorial.
A decription of what each missing command does is included in the script.
You should be able to consult git documentation and figure out what each command is asking for.

To run the script type:

```
source <your wm user id>.sh
integrate
```

Naturally, the path to the script may change depending on the directory you are in.

Once you have added a few lines to the script and have executed it, you may be interested in checking that it is working.  For starters, try running
```
git status
```
to ensure that all files are tracked.  Additionally, log in to the gitlab page for this homework assignment and verify that your changes appear on both the master branch and your personal branch.

When you run the script you might get an error message something like this:
```
remote: 
remote: To create a merge request for kmnelson, visit:
remote:   https://gitlab.com/kmnelson/continuous_integration_spring2017/merge_requests/new?merge_request%5Bsource_branch%5D=kmnelson
remote: 
```
This can be ignored.  Gitlab doesn't like when a user merges from a personal branch from the command line.
The script is designed to work around this, but gitlab doens't understand that so it complains.


#### Continuous Integration and Test Cases
We have provided some buggy code and a few test cases. You will be implementing a CI system to automatically build and test the code. 
The test cases provided are not sufficient to determine the errors in the code, you will be augmenting them with test cases of your own.

There will be three simple bugs within the code.

Create test cases to find bugs.

Don't forget to commit along the way!


### What you need to provide
1. Completed git script
2. Fixed code
3. Test cases
4. At least 8 commits, including one for each bug found (3) and test case added (5 minimum)




------
<a href="https://docs.google.com/a/email.wm.edu/presentation/d/1hZBSJoL62TAn-Lbcpkq_6wkzmwGd7jujaWRboyxbQHs/edit?usp=sharing">Here are the presentation slides!</a>

## Benefits of CI
* Reduces time spent in deployment and development
* Faster system-wide feedback from effects of local changes
* Less manual testing
* Earlier detection of bugs
* Recent code is shared -- helps collaboration

### Best Practices
* Maintain a code repository
* Automate your build
* Make your build self-testing
* Daily commits to the baseline by everyone on the team
* Every commit (to the baseline) should be built
* Keep your builds fast
* Clone the production environment and test there
* Make it easy to get the latest deliverables
* Everyone on the team can see the results of your latest build
* Automate build deployment


