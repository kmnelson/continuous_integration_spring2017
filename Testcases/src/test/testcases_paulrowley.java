package test;

// IMPORTANT: Modify the class name to match your user id
public class testcases_paulrowley {
    public static void main(String[] args) {
	// The way pass/fail is designated is by system.exit
	// feel free to use different exit values to indicate differnt
	// failed tests and build out the shell script accordingly,
	// but all that is required is to exit on 0 for success
	// and exit on 1 for failure
        
        if(buggedFactorial_paulrowley.findFactorial(0) != 0)
            System.exit(1);
        
        if(buggedFactorial_paulrowley.findFactorial(1) != 1)
            System.exit(1);
        
        if(buggedFactorial_paulrowley.findFactorial(2) != 2)
            System.exit(1);
        
        if(buggedFactorial_paulrowley.findFactorial(5) != 5*4*3*2*1)
            System.exit(1);
        
        if(buggedFactorial_paulrowley.findFactorial(10) != 10*9*8*7*6*5*4*3*2*1)
            System.exit(1);
        
        
	System.exit(0);
    }
}
