package test;

// IMPORTANT: Modify the class name to match your user id
public class testcases_<your wm user id> {
    public static void main() {
	// The way pass/fail is designated is by system.exit
	// feel free to use different exit values to indicate differnt
	// failed tests and build out the shell script accordingly,
	// but all that is required is to exit on 0 for success
	// and exit on 1 for failure
	System.exit(0);
    }
}
