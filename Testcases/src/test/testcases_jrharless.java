package factorial;

public class buggedFactorial_paulrowley {
	public static int findFactorial(int n){
        int sum = n; //error one is a semi colon problem
		while (n != 0) {
			n--;
			sum *= n; //error 3, should be multiply not add
		}
		return sum;
	}


	public static void main(String[] args) { //error 2, main string args
		System.out.print(findFactorial(5));
	}
}

