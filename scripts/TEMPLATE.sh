integrate() 
{
    #--------------------------------------------
    # IMPORTANT:
    # Before you get started using this script, run the following commands in a directory of your choosing:

    # git clone https://gitlab.com/kmnelson/continuous_integration_spring2017.git
    # cd continuous_integration_spring2017
    # git branch <your wm user id>
    # cp scripts/TEMPLATE.sh scripts/<your wm user id>.sh

    # This copies the git repository for this homework, sends you
    # into the directory where the git project is, creates a personal
    # branch, and copies the template of this script into a version you
    # can modify.
    # DO NOT MODIFY the TEMPLATE.sh file



 
    #--------------------------------------------
    # Once your are done with the modifications this 
    # script can be executed in the command line by sourcing
    # it and then typing into the command line:

    # source <your wm user id>.sh
    # integrate

    


    #--------------------------------------------
    #
    # TASK 1.
    #
    # Fill in the following fields with the location of your git repository locally
    # fill in the WM_USERID field with your user id
    # (you will be graded on the commits on your branch identified by your userid)
    #--------------------------------------------
    SOURCE_DIR=/THE/DIRECTORY/YOU/CLONED/THE/REPO/IN/continuous_integration_spring2017
    WM_USERID="<your wm user id>"


    #--------------------------------------------
    # This is a sanity check to make sure you are in the right place.
    # No need to modify anything here
    #--------------------------------------------
    START_DIR=`pwd`
    cd ${SOURCE_DIR}



    #--------------------------------------------
    # The script is designed to test your code before integrating
    # these lines will execute a test suite that you will modify later
    # and based on the exit value it will ask for user input to override
    # test failures and commit anyway.
    # In a larger project this would prevent you from pushing 
    # broken code to the master branch
    #
    # Nothing to modify here, carry on.
    #--------------------------------------------

    cd ${SOURCE_DIR}/Testcases/bin/
    javac -d . ${SOURCE_DIR}/Testcases/src/test/testcases_${WM_USERID}.java
    java -cp . test.testcases_${WM_USERID} 
    EXIT_STATUS=$?
    echo ${EXIT_STATUS}
    if test ${EXIT_STATUS} -eq 0; then
	echo "Your code passed all tests.  Commiting changes"
    else
	while true; do
	    read -p "Your code failed some tests.  Would you like to commit anyway?" USER_INPUT
	    case "${USER_INPUT}" in 
		"y") EXIT_STATUS="0"; break;;
		"Y") EXIT_STATUS="0"; break;;
		"n") EXIT_STATUS="99"; break;;
		"N") EXIT_STATUS="99"; break;;
		*) echo "Respond y or n";;
	    esac
	done
    fi

    if test ${EXIT_STATUS} -eq 0; then


	
        #--------------------------------------------
	#
        # TASK 2.
	#
        # Insert one line of code here to switch to the branch identified
        # by your wm user id as stored in the variable you set above
        #--------------------------------------------



        #--------------------------------------------
	#
        # TASK 3.
	#
        # Insert one line of code here to stay up to date with master
        # hint: here you want to stay up to date with gitlab... you 
        # will need a url to specify that similar to the way you first
        # cloned the repository
        #--------------------------------------------

	
    
        #--------------------------------------------
	#
        # TASK 4.
	#
        # Insert one line of code here to upload all of your changes to the 
        # staging area from the working directory
        # This is important because it adds untracked files to git's list 
        # of known files
        #--------------------------------------------


	
        #--------------------------------------------
	#
        # TASK 5.
	#
        # Insert one line of code to commit all changes in your staging area
        # to your branch
        #
        # Note: when this command executes there will be a prompt for a message
        # to identify your changes.  If you do not input a message then git will
        # not accept your changes.  It is good practice to put a message anyway
        # so that you can remember what changes you made and why
        #--------------------------------------------



        #--------------------------------------------
	#
        # TASK 6.
	#
        # Here things get a little more complicated.  Gitlab doesn't like
        # to allow users to merge branches very easily from the command line.
        # In order to keep your own branch and the master up to date with each 
        # other you must push on both the master branch and your personal branch
        #
        # Insert three lines of code here to 
        # switch to the master branch
        # merge your personal branch into the master branch (using the variable you set earlier)
        # push changes (url not required here)
        #
        #--------------------------------------------



        #--------------------------------------------
	#
        # TASK 7.
	#
        # Insert two lines of code to return to your own branch and
        # upload your changes to gitlab (url to specify repository goes here in addition to the wmuserid variable)
        #
        # Note: when this command executes it will make a fuss about 
        # a merge request.  Just ignore that, we fixed it in part 5
        # by merging locally and pushing the master branch
        #--------------------------------------------




    #--------------------------------------------
    # This prints a simple statement to show that the user 
    # selected not to commit their changes because of a failed 
    # test case
    #
    # Next it returns to previous directory
    #
    # nothing to modify here
    #--------------------------------------------
    elif test ${EXIT_STATUS} -eq 99; then
	echo "User aborted code integration"
    fi
    cd ${START_DIR}
}